#include <stdio.h>
#include <stdlib.h>

//Funcion para contar el numero de cifras de nuestro numero

int numCifras (int num){
	int aux = num;
	int cont = 0;

	while(aux != 0){
		aux = aux/10;
		cont++;
	}

	return cont;
}

int pow(int base, int exponente){
	int elevado = 1;
	for(int i= 0; i<exponente; i++){
		elevado = elevado * base;
	}
	return elevado;
}

int main () {
	int n1;
	int ultimaCifra;
	int numFinal;
	int aux;
	int nCifras;
	int cont;

	printf("Dame un numero: ");
	scanf("%d",&n1);
	
	nCifras = numCifras(n1);
	aux = n1; 
	numFinal = 0;
	cont = 1;
	while(aux != 0){
		ultimaCifra = aux % 10;
		aux = aux / 10;
		numFinal = numFinal + ultimaCifra * pow(10, (nCifras-cont));
		cont++;
	}
	

	/*printf("Ultima cifra: %d \n",ultimaCifra);
	 
	 = ultimaCifra * pow(10, (nCifras-1));
	ultimaCifra = (n1/10) % 10;

	numFinal = numFinal + ultimaCifra * pow(10, (nCifras-2));*/

	printf("Numero final: %d \n",numFinal);

	if(numFinal == n1){
		printf("El numero es capicua\n");
	}else{
		printf("El numero no es capicua\n");
	}

	return 0;
}