#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int numCaracteres (char palabra[]){
	int cont = 0;
	int i = 0;
	while(palabra[i] != '\0'){
		i++;
		cont++;
	}
	return cont;
}

int main (){
	char palabra[20];
	char palabraAlReves[20];
	int nCaracteres;

	printf("Inserte una palabra: ");
	scanf("%s",palabra);

	nCaracteres = numCaracteres(palabra);
	printf("Numero de caracteres: %d \n", nCaracteres);
	int j = 0;

	for(int i = nCaracteres; i>0; i--){
		printf("Entro en el bucle: %c\n", palabra[i-1]);
		palabraAlReves[j] = palabra[i-1];
		j++;
		if(i==1){
			palabraAlReves[j] = '\0';
		}
	}
	
	printf("Palabra alreves: %s \n", palabraAlReves);	

	if(strcmp(palabraAlReves,palabra) == 0){
		printf("Son palindromas.\n");
	}else{
		printf("No son palindromas.\n");
	}
	

	return 0;
}