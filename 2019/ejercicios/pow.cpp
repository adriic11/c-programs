#include <math.h>
#include <stdio.h>
int main(int argc, char const *argv[])
{
	int numero, potencia; // Aquí guardaremos lo que ingrese el usuario
	printf("Ingresa el numero:\n");
	scanf("%d", &numero);
	printf("Escribe la potencia para elevarlo:\n");
	scanf("%d", &potencia);
	int elevado = 1;
	for(int i= 0; i<potencia; i++){
		elevado = elevado * numero;
	}
	printf("%d elevado a la potencia %d es %d\n", numero, potencia, elevado);
	return 0;
}