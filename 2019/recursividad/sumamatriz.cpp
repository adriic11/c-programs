#include <stdio.h>
#include <stdlib.h>

int main () {
	int matriz[3][3];
	int suma = 0;
	int sumaImp = 0;
	int matrizPar = 0;

	for (int i = 0; i < 3 ; ++i) { 		//for para las filas.
		for (int j = 0; j < 3; j++){ 	//for para columnas
			printf("matriz[%i][%i]= ", i, j);
			scanf("%i",&matriz[i][j]);
		}
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			printf("%i ",matriz[i][j]);
		}
		printf("\n");
	} 

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			suma = suma + matriz[i][j];		
		}
	}

	for (int i = 0; i < 3; i++) {              //for para sumar impares
		for (int j = 0; j < 3; j++) {
			if (matriz[i][j] % 2 != 0) {
				sumaImp = sumaImp + matriz[i][j];	
			}	
		}
	}	

	for (int i = 0; i < 3; i=i+2) {
		for (int j = 0; j < 3; j=j+2) {
			matrizPar = matrizPar + matriz[i][j];
		}
	}

	printf("La suma es igual: %i\n",suma);
	printf("La suma con impares es igual: %i\n",sumaImp);
	printf("La suma de la matriz par es: %i\n",matrizPar);

	return 0;
}