#include <stdio.h>
#include <stdlib.h>
//5! = 5 * 4 * 3 * 2 * 1
//5! = 5 * 4! = 5 * 4 * 3! = 5 * 4 * 3 * 2! = 5 * 4 * 3 * 2 * 1! = 5 * 4 * 3 * 2 * 1 
//! de numeros negativos no existe
//1! = 1

int fact(int n){
	if(n==1){
		return 1;
	}else{
		return n * fact(n-1);
	}
}


int main () {
	int n;

	printf("Indicame el número: ");
	scanf("%i",&n);
	printf("El factorial de %i es: %i\n ",n,fact(n));

	return 0;

}