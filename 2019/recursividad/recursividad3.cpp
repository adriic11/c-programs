//Serie Fibonacci sin for.

#include <stdio.h>
#include <stdlib.h>

//[1,2,3,4,5] = 15
//[1] = 1
//[3] = 3
//[5,7] = 7 + [5] = 7 + 5
//[4,8,12] = 12 + [4,8] = 12 + 8 + [4] = 12 + 8 + 4
//sumaArray([4,8,12], 3) = 12 + sumaArray([4,8,12], 2) = 12 + 8 + sumaArray([4,8,12], 1) = 12 + 8 + 4 

int sumaArray(int array[], int tam) {
	if(tam == 1){
		return array[0];
	}else{
		return array[tam-1] + sumaArray(array, tam-1);
	}
}

int sumaImparesArray(int array[], int tam) {
	if(tam == 1){
		if(array[0] % 2 == 1){
			printf("Array[0] = %i\n", array[0]);
			return array[0];	
		}else{
			printf("0\n");
			return 0;
		}
	}else{
		if(array[tam-1] % 2 ==1){
			printf("Impar - Array[tam-1] = %i\n", array[tam-1]);
			return array[tam-1] + sumaArray(array, tam-1);	
		}else{
			printf("Par - Array[tam-1] = %i\n", array[tam-1]);
			return sumaArray(array, tam-1);	
		}
		
	}
}

int main () {
	int numeros[5];

	for (int i = 0; i < 5; ++i)
	{
		printf("Indícame un número: ");
		scanf("%i",&numeros[i]);
	}

	printf("Suma del Array: %i\n",sumaArray(numeros, 5));
	printf("La suma de los impares es: %i\n",sumaImparesArray(numeros, 5));


	return 0;
}