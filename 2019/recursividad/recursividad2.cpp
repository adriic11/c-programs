#include <stdio.h>
#include <stdlib.h>

// Serie de Fibonacci
//1 1 2 3 5 8 13...
//fibo(4) = fibo(3) * fibo(2) = (fibo(2) + fibo(1)) + 1 = (1 +1) + 1 = 3

int fibo (int pos) {
	if(pos == 1 || pos == 2){
		return 1;
	}else{
		return fibo(pos-1) + fibo(pos-2);
	}
}

int main () {
	int pos;

	printf("Indícame la posición de la que quieres el térmido de Fibonacci: ");
	scanf("%i",&pos);

	printf("El término %i de la serie de Fibonacci es : %i \n",pos,fibo(pos));

	for(int i = 1; i<=10; i++){
		printf("El término %i de la serie de Fibonacci es : %i \n",i,fibo(i));
	}

	return 0;
}