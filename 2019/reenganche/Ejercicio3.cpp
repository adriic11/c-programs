#include <stdio.h>
#include <stdlib.h>
#define CIEN 100

int main (){
	const int PASO = 3;
	int inicio;
	printf("Indicame el numero: ");
	scanf("%d",&inicio);
	int fin = inicio + CIEN;

	printf("Con un for.\n");
	for(int i=inicio; i < fin; i = i + PASO){
		printf("%d  ", i);
	}
	printf("\n");

	printf("Con un while.\n");
	int i = inicio;
	while(i < fin){
		printf("%d  ",i);
		i = i + PASO;
	}
	printf("\n");

	printf("Con un do-while.\n");
	i = inicio;
	do{
		printf("%d  ",i);
		i = i + PASO;
	}while(i < fin);

	printf("\n");


	return 0;
}