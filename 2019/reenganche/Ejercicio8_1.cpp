#include <stdio.h>
#include <stdlib.h>
#define INICIO 2
#define FIN 100

int main (){
	const int PASO = 3;
	
	int suma = 0;

	for(int i=INICIO; i < FIN; i = i + PASO){
		suma=(i%5==0)?(suma+i):suma;
	}
	printf("La suma es: %d\n", suma);

	
	return 0;
}