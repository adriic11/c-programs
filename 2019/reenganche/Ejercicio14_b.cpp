#include <stdlib.h>
#include <stdio.h>

//Funcion que calcula el tamaño de la frase
int longitudStr(char frase[]){ 
	int i=0;

	while(frase[i] != '\0'){
		i++;
	}
	return i;
}

//Funcion que calcula la cantidad de la frase que no son mayúsuculas
int contadorNoMayuscula(char frase[]){
	int contMinusculas = 0;
	int tam = longitudStr(frase);
	for(int i = 0; i<tam; i++){
		if((frase[i] < 'A' || frase[i] > 'Z') && frase[i]!= 'Ñ' && frase[i]!= ' '){
			contMinusculas++;
		}
	}
	return contMinusculas;
}
//Función que modifica las mayusculas a minusculas y la imprime.
void imprimirFrase(char frase[]){
	int tam = longitudStr(frase);
	for(int i = 0; i<tam; i++){
		if(frase[i] != ' ' && frase[i] != 'Ñ'){
			frase[i] = char(int(frase[i])+32);	
		}else{
			if(frase[i]=='Ñ'){
				frase[i] = char(int(frase[i])-1);
			}
		}		
	}
	printf("La frase en minúsculas es: %s\n", frase);
}


int main () {
	char frase[50];

	int contMinusculas;
	
	do{
		printf("Indicame la frase en mayúsuculas: ");
		scanf("%100[^\n]",frase);
		contMinusculas = contadorNoMayuscula(frase);	
		printf("ContadorMinusculas: %d\n", contMinusculas);
		if(contMinusculas != 0){
			printf("Frase no válida, tienen que ser todas mayúsculas.\n");
			getchar(); //Si contiene minusculas, vuelva a pedir la frase y vacie buffer.
		}
	}while(contMinusculas != 0);

	imprimirFrase(frase);
	
	return 0;
}