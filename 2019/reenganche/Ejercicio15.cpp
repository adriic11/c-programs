#include <stdlib.h>
#include <stdio.h>

//Funcion que calcula el tamaño de la frase
int longitudStr(char frase[]){ 
	int i=0;

	while(frase[i] != '\0'){
		i++;
	}
	return i;
}

//Función que modifica las mayusculas a minusculas y la imprime.
void imprimirFrase(char frase[]){
	int tam = longitudStr(frase);
	for(int i = 0; i<tam; i++){
		if(frase[i] != ' ' && frase[i] != 'Ñ'){
			frase[i] = char(int(frase[i])+32);	
		}else{
			if(frase[i]=='Ñ'){
				frase[i] = char(int(frase[i])-1);
			}
		}		
	}
	printf("La frase en minúsculas es: %s\n", frase);
}


int main () {
	char frase[50] = "BIENVENIDO AL PROGRAMA";
	imprimirFrase(frase);
	
	return 0;
}