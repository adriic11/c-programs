#include <stdio.h>
#include <stdlib.h>

int main (){
	char texto[100];
 	
 	int i = 0;
 	int letras = 0;
 	int numeros = 0;

 	printf("Inserta un texto: ");
 	scanf("%100[^\n]", texto);

	while(texto[i] != '\0'){
		if(((int)texto[i]) >= (int)'0' && ((int)texto[i]) <= (int)'9'){
			numeros++;
		} 
		if(((int)texto[i]) >= (int)'a' && ((int)texto[i]) <= (int)'z' || ((int)texto[i]) >= (int)'A' && ((int)texto[i]) <= (int)'Z'){
			letras++;
		}
		i++;
	}
	printf("El numero de letras es: %d\n", letras);
	printf("La cantidad de numeros es: %d\n", numeros);

	return 0;
	
}