#include <stdio.h>
#include <stdlib.h>

int main(){
	char palabra[100];
	char copia[100];
	int i = 0; //Se usa para recorrer la palabra en el while
	int k = 0;
	//Pedimos la palabra
	printf("Dime tu palabra: \n");
	scanf("%s",palabra);
	
	//Calculamos el tamaño de la misma, el útlimo caracter cuando se pulsa enter es \0

	while(palabra[i] != '\0') {//Va recorriendo la palabra hasta encontrar el \0
		i++; 
	}

	for(int j = (i-1); j>=0; j--) {
		copia[k] = palabra[j];
		k++;
	}
	copia[k] = '\0';
	printf("Tu palabra al revés: %s\n",copia);

	return 0;

}
