#include <stdio.h>
#include <stdlib.h>
#define INICIO 2
#define FIN 100

int main (){
	const int PASO = 3;
	printf("Con un for.\n");
	for(int i=INICIO; i < FIN; i = i + PASO){
		printf("%d  ", i);
	}
	printf("\n");

	printf("Con un while.\n");
	int i = INICIO;
	while(i < FIN){
		printf("%d  ",i);
		i = i + PASO;
	}
	printf("\n");

	printf("Con un do-while.\n");
	i = INICIO;
	do{
		printf("%d  ",i);
		i = i + PASO;
	}while(i < FIN);

	printf("\n");


	return 0;
}