#include <stdio.h>
#include <stdlib.h>

int main(){
	char palabra[100];
	char copia[100];
	int i = 0;
	int k = 0;

	printf("Escribeme la palabra : ");
	scanf("%s",palabra);

	while(palabra[i] !='\0'){
		i++;
	}
	for(int j = (i-1);j >= 0;j--){
		copia[k] = palabra[j];
		k++;
	}
	printf("La palabra %s se escribe al revés: %s\n",palabra,copia);

	return 0;
}