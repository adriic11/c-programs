#include <stdlib.h>
#include <stdio.h>

int main(){
	float numero;
	float sum = 0;
	int contador = 0; //contador

	do{
		printf("Indicame el número: ");
		scanf("%f",&numero);
		if(numero>=0){
			sum += numero;
			contador++;	
		}	
	}while(numero >= 0);
	if(contador > 0){
		printf("La media es igual: %f\n",sum/contador);	
	}else{
		printf("No se ha insertado ningún numero positivo.\n");	
	}
	
	return 0;
}