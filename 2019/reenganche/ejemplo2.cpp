#include <stdio.h>
#include <stdlib.h>

int main(){
	char palabra[100];
	int i = 0;
	char aux;
	
	printf("Indicame la palabra: ");
	scanf("%s",palabra);

	while(palabra[i] != '\0') {
		i++;
	}
	int k = (i-1); //para almacenar ultima penultima letra
	for(int j = 0;j < (i/2); j++){ // Va a realizar los intercambios de letras: Primer con última, segunda con penultima...
		aux = palabra[j];
		palabra[j] = palabra[k];
		palabra[k] = aux;
		k--;
	}
	printf("La palabr al revés es: %s\n",palabra);

	return 0;
}