#include <stdio.h>
#include <stdlib.h>

int main(){
	int temperatura;

	printf("Dime el grado de temperatura: ");
	scanf("%d",&temperatura);
	int estado;
	//-20 hielo, 0 liquido > 42 vapor

	if(temperatura <= -20){
		estado = 1; //hielo
	}else{
		if(temperatura >-20 && temperatura< 42){
			estado = 2; //liquido
		}else{
			estado = 3;
		}
	}

	switch(estado){
		case 1:
			printf("Su estado es hielo.\n");
			break;
		case 2:
			printf("Su estado es líquido.\n");
			break;
		case 3:
			printf("Su estado es vapor.\n");
			break;
	}

	return 0;
	
}