#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {

	char nombre[20];
	char apellido1[20];
	char apellido2[20];
	int edad;

	for (int i = 0; i < 3; ++i)
	{
		
		printf("Usuario %d Digame su nombre: ", (i+1));
		scanf("%s", nombre);

		printf("Digame su apellido: ");
		scanf("%s", apellido1);

		printf("Digame su segundo apellido: ");
		scanf("%s", apellido2);


		printf("¿Cuantos años tienes?: ");
		scanf("%i", &edad);

		if(edad>18){
			printf("Bienvenido %s %s %s. Tu edad es %i, eres mayor de edad.\n",nombre,apellido1,apellido2,edad);	
		} else {
				printf("Bienvenido %s %s %s. Tu edad es %i, eres menor de edad.\n",nombre,apellido1,apellido2,edad);	
		}
	}

	return 0;
}