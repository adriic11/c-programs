//3.Pregúntale el nombre al usuario y almacénalo en un array de caracteres.

#include <stdio.h>
#include <stdlib.h>

#define N 15

int
main (int argc, char *argv[])
{
  char nombre[N];
  
  printf("¿Cuál es tu nombre de usuario?\n");
  scanf("%s", nombre);

  printf("Tu nombre es %s.\n", nombre);
    return EXIT_SUCCESS;
}

