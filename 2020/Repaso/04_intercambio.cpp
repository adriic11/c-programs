//04.Escribe un programa que intercambie dos números.

#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
  unsigned n1,n2;
  unsigned aux;

  printf("Indicame el primer número: ");
  scanf("%u", &n1);

  printf("Indicame el segundo número: ");
  scanf("%u", &n2);

  //Intercambio de valores con aux.
  aux=n1;
  n1=n2;
  n2=aux;

  printf("El primer número ahora es igual a: %u\n", n1);
  printf("El segundo número ahora es igual a: %u\n", n2);

    return EXIT_SUCCESS;
}

