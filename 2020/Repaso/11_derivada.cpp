#include <stdio.h>
#include <stdlib.h>

#define NCOEF 6
#define GRADO (NCOEF -1)

#define STEP .001   //0,01
#define LI -10
#define LS 10

#define TOTAL_PUNTOS (int) ( (LS - LI) / STEP )
//si empieza en menos 10 y acaba en 10, convertir la x a indice.

enum TStrategy { st_text, st_curve };
const int print_strategy = st_text;

double
f (double p[NCOEF], double x) {
    double valor = 0;
    double pot   = 1;

   for (int coef=0; coef<NCOEF; coef++) {
      valor += pot * p[coef];
      pot *= x;
   }

  return valor;
} 

/*Calculate the index associated to a given x*/
int
x2i (double x) {
    return (int) ( (x - LI) / STEP );
}

void
print_text (double raster[TOTAL_PUNTOS + 1])     //Imprime todos los valores del array
{

    for (double x=LI; x<=LS; x+=STEP)
        printf ("%.2lf\t%.2lf\n", x, raster[x2i (x)]);
}

void 
print_curve (double raster[TOTAL_PUNTOS])
{
}

void
print (double raster[TOTAL_PUNTOS + 1]) {
    switch (print_strategy) {
        case st_text:
            print_text (raster);
            break;
        case st_curve:
            print_curve (raster);
            break;
    }

}


int
main (int argc, char *argv[])
{
    double p[NCOEF] = { -3, 2, 5, 1 };
    double raster[ TOTAL_PUNTOS + 1 ];      //Pasar de la formula concreta a una lista que acumula los valores en cada punto
    double deriva[ TOTAL_PUNTOS ];
    //Rellenar los valores de la función
    for (double x=LI; x<LS + STEP; x+=STEP)  
        raster[x2i (x)] = f(p, x);          //Voy metiendo lo que valga la funcion en el punto x   f(p, x)-> Calcula la altura de un polinomio en x
                                            //raster [x2i (x)]-> En que celda del array lo tengo que poner, tu le das valores de x y el te dice que posicion ocupa
    
    //Calcular la derivada
    for (double x=LI; x<LS; x+=STEP)
        deriva[x2i (x)] = (raster[ x2i (x+STEP)] - raster[ x2i (x)]) /STEP;


    for (double x=LI; x<LS; x+=STEP)
        if ( raster[ 2xi (x) ] * raster[ x2i (x+STEP) ] <= 0 )
            printf ("Zero: %.4lf\n", x);

    print (raster, deriva);                         //Imprime los valores que hay escritos en el array
    

    return EXIT_SUCCESS;
}

//Recorrer raster y decir en que puntos corta, meterlo en un array que se llame zero -> en que x estan los 3 ceros de la funcion
