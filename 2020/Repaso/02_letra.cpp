//2.Pregúntale al usuario su letra preferida y almacenala en una variable.

#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    char letra;

    printf("¿Cuál es tu letra favorita?\n");
    scanf("%c",&letra);

    printf("Tu letra favorita es %c.\n",letra);

    return EXIT_SUCCESS;
}

