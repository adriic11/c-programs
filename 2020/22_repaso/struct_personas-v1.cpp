#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 20

struct Tpersona {
    char nombre[N];
    int edad;
    char cromos[20];
};

int
main (int argc, char *argv[])
{
    struct Tpersona persona1;

    printf ("Indicame su nombre: ");
    scanf ("%s", persona1.nombre);

    printf ("Indicame su edad: ");
    scanf ("%i", &persona1.edad);

    printf ("%s tiene %i años.\n", persona1.nombre, persona1.edad);
    return EXIT_SUCCESS;
}

