#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct formulario {
    char nombre[20];
    char apellido[20];
    int edad;
};

void pedir (formulario *f);
void imprimir (formulario *f);

int main () {

    struct formulario f;

    pedir (&f);
    imprimir (&f);

    return 0;
}

void pedir (formulario *f) {

    printf ("Indicame su nombre: ");
    scanf ("%s",f->nombre);

    printf ("Indicame su apellido: ");
    scanf ("%s",f->apellido);

    printf ("Indicame su edad: ");
    scanf ("%i",&f->edad);
}

void imprimir (formulario *f) {

    printf ("Tu nombre es: %s\n", f->nombre);
}
