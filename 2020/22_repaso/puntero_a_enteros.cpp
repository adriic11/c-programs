#include <stdio.h>
#include <stdlib.h>

#define MAX_ENTEROS 10
int
main (int argc, char *argv[])
{

    int enteros[MAX_ENTEROS];
    int *puntero_a_enteros;
    puntero_a_enteros=enteros;
     
    for (int i = 0; i<MAX_ENTEROS; i++){
    enteros[i]= i;
    }


    for (int i = 0; i<MAX_ENTEROS; i++){
    printf ("Num en posicion [%i]: %i \n", i, enteros[i]);
    }


    do {
    printf ("Aquello a lo que apunta: %i \n", *puntero_a_enteros);
    puntero_a_enteros++;
    } while (*puntero_a_enteros < 7);
    return EXIT_SUCCESS;
}
