#include <stdio.h>
#include <stdlib.h>


int es_borde (int fila, int col, int lado){
    if (fila == 0 || fila == (lado-1) || col == 0 || col == (lado-1)){
    return 1;
    } else {
    return 0;
    }

}

void pinta_fila (int fila_actual, int lado){
    
    if (fila_actual == lado){
        printf("\nFIN\n");
        return;
    } else {
      printf ("\n");
      for (int col = 0; col<lado; col++){
            if (es_borde(fila_actual, col, lado)==1){
                printf ("#");
            } else {
                printf (" ");
      
        }
      }
        fila_actual++;
        pinta_fila(fila_actual, lado);
    }
 
}


int
main (int argc, char *argv[])
{
    int fila_actual = 0;   
    int lado;
    printf ("dime lado: ");
    scanf ("%i", &lado);
    
    pinta_fila(fila_actual, lado);
      printf ("\n");

    return EXIT_SUCCESS;
}
