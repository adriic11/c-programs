#include <stdio.h>
#include <stdlib.h>


int es_borde (int fila, int col, int lado){
    if (fila == 0 || fila == (lado-1) || col == 0 || col == (lado-1)){
    return 1;
    } else {
    return 0;
    }

}

int
main (int argc, char *argv[])
{
    
    int lado;
    printf ("dime lado: ");
    scanf ("%i", &lado);
  for (int fila = 0; fila<lado; fila++){
    printf ("\n");

    for (int col = 0; col<lado; col++){
      if (es_borde(fila, col, lado)==1){
        printf ("#");
      } else {
        printf (" ");
      
      }
    }
  }
      printf ("\n");

    return EXIT_SUCCESS;
}
