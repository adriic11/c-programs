#include <stdio.h>
#include <stdlib.h>

void pintalos(int *puntero_a_enteros, int cuantos){
    for (int i = 0; i<cuantos; i++){
        printf ("Num en posicion [%i]: %i \n", i, *puntero_a_enteros);
        puntero_a_enteros++;
    }

    puntero_a_enteros -= cuantos;                                   //P vuelve a la primera posicion (0) Aritmetica de punteros
}

int *incrementa_en_uno (int *p_to_i, int *cuantos){
    //The movie (Forzando manualmente el incremento del tamaño de P para un nuevo int)
    p_to_i = (int*) realloc ( p_to_i, (*cuantos+1) * sizeof(int)); //Tamaño reservado para P incrementa en 1 manualmente
    *cuantos += 1;                                                 //La cantidad de números almacenados aumenta en uno
    
    p_to_i += (*cuantos-1);                                        //P se coloca en la ultima posicion (cantidad de numeros -1)
    printf ("Mete número para ultima posicion: ");
    scanf ("%i", p_to_i);                                          //Se guarda un numero en la ultima posicion
    p_to_i -= (*cuantos-1);                                        //Retrocedemos la misma cantidad que avanzamos (Vuelta al inicio)
    return p_to_i;
}

int *devuelve (int cuantos){

    int *p = (int*) malloc (cuantos * sizeof (int));           //P requiere espacios para guardar ints, cuantos * sizeof(int) dime donde
    return p;                                                 //Aquí, habitacion 202, posee (cuantos)dormitorios para ints
}
    
    
int
main (int argc, char *argv[])
{

    int cuantos;
    int *puntero_a_enteros = NULL;                            //P = 0

    printf ("Para cuantos?: ");
    scanf ("%i", &cuantos);

    puntero_a_enteros=devuelve(cuantos);                      //P = 202 |int|int|int|int|int|cuantos...

    for (int i = 0; i<cuantos; i++){
    printf ("Mete número para posicion [%i]", i);
    scanf ("%i", puntero_a_enteros);                      
    puntero_a_enteros++;                                     //Siguiente dormitorio para int 
    }

    puntero_a_enteros -= cuantos;                            //Vuelta a la puerta de la habitacion 202

    pintalos(puntero_a_enteros, cuantos);

    puntero_a_enteros=incrementa_en_uno(puntero_a_enteros, &cuantos);

    pintalos(puntero_a_enteros, cuantos);
    
    free(puntero_a_enteros);                                                          //Violacion de segmento en la siguiente ejecucion (20-30% prob) WHY?
    return EXIT_SUCCESS;
}
