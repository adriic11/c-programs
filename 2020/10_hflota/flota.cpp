#include <stdio_ext.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#define DIM 8

#define AGUA   0
#define BARCO  1
#define PECIO  2

#define R_AGUA   0         //R=RESULTADO
#define R_TOCADO 1

#define DISPARO 1

#ifndef NDBUG
#define DEBUG(...)                \
    fprintf ("\x1b[32m")          \
    fprintf (stderr, __VA_ARGS__);\
    fprintf (stderr, "\n");       \
    fprintf (stderr, "\x1b[0m");  \
    __fpurge (stdin);             \
    getchar ();
#else
#define DEBUG(...)
#endif
//fpurge -> Borra entrada standar


unsigned jugador (unsigned turno) { return turno % 2; } //Devuelve turno resto 2.


void
cabecera (turno) {
    system ("clear");
    system ("toilet -fpagga --gay HUNDIR LA FLOTA");
    printf ("\n\n");
    printf ("Turno %u\t\tJugador %u\n", turno + 1, jugador (turno) + 1);
    printf ("\n\n");
}

unsigned
barcos_quedan () 
{
    return 1;
}

unsigned 
queden_barcos () 
{
    return barcos_quedan ();
}

/*OPERACION DE REFERENCIA Y DIRECCION */
void
preguntar_coordenadas (unsigned *fila, unsigned *col) 
{
    // toDo -> Comprobar si el disparo esta en el rango.
    // toDo -> Comprobar que no hubiera disparo ahí.
    printf ("Fila: ");
    scanf ( "%u", fila);
    printf ("Columna: ");
    scanf ( "%u", col);
}

void
eliminar_barcos ()
{

}

void 
imprimir (unsigned tablero [DIM][DIM]) {

    for (int f=0; f<DIM; f++) {
        printf ("\t");
        for (int c=0; c<DIM; c++)
            switch (tablero [f][c]) {
                case AGUA:
                    printf ();
                    break;
                case BARCO
                    printf ();
                    break;
                case PECIO:
                    printf ();
                    break;
            }

        printf ("\n");
    }
    
    printf ("\n");
}

unsigned 
procesar_disparos (unsigned fila, unsigned col, unsigned tablero[DIM][DIM])
{
      //Comprobar si hay barco activo
      if (tablero [fila][col] == BARCO)
          tablero [fila][col] == PECIO
      //Si has acertado  eliminar el barco
      //Devolver el resultado del disparo
}

int
main (int argc, char *argv[])
{ 
    /*DECLARACION DE VARIABLES*/
    unsigned turno = 0;
    unsigned fila, col;
    unsigned tablero[2]DIM[][DIM] = {
        /*INICIALIZACION*/
        {
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0},    
            {0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 1, 1, 1, 0, 0},
          },
            
          {
            {0, 0, 0, 1, 1, 0, 0, 0},
            {0, 0, 1, 0, 0, 1, 0, 0},
            {0, 0, 0, 0, 0, 1, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 0, 0},
            {0, 0, 1, 1, 1, 1, 0, 0},
          }
    };
    unsigned disparos[2][DIM][DIM];

    bzero (disparos, sizeof (disparos));
   // Otra manera
   // for (int t=0; t<2; t++)
   //     for (int f=0; f<DIM; f++)
   //         for (int c=0; c<DIM; c++)
   //             tablero[t][f]][c] = 0;

   // bzero (tablero, sizeof (tablero));  bzero -> En esa direccion de memoria va a rellenar todo con ceros
    
    /*BUCLE DE JUEGO*/
    while (queden_barcos ()) {
        cabecera (turno); 
        imprimir (tablero [jugador (turno)] );    /*tablero contiene la direccion de la primera celda, tablero [0] ,tablero [0][0] ,&tablero[0][0][0] */
        preguntar_coordenadas (&fila, &col);
        DEBUG ("Coordenadas : [%u][%u]\n", fila, col);
        turno++;
        procesar_disparos (fila, col, tablero[ jugador (turno) ]);
    }

    return EXIT_SUCCESS;
}

