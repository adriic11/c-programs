#include <stdio.h>
#include <stdlib.h>

int main () 
{
	char a;		/* Variable 'a' de tipo char */
	char *pchar;	/* Puntero a char 'pchar' */	

	pchar = &a;	/* 'pchar' <- @ de 'a' */

	printf("la direccion de memoria de 'a' es: %p \n", &a);
	printf("y su contenido es : %c \n", *pchar);
	
	return 0;
}
