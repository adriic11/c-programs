#include <stdlib.h>
#include <stdio.h>

#define  N 10

unsigned ticket () {
    static int num = 0;

    return ++num;
}

int main () {
    
    for (int i=0; i<N; i++)
        printf ("Su numero de ticket es: %u\n", ticket());

    return EXIT_SUCCESS;
}
