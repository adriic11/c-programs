#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

//Hacer un programa que nos pregunte si queremos continuar y
//hasta que no escribamos la letra s no pare.

int main (int argc, char *argv[]) {
    
    char c;

    do {
        printf ("Escribe una letra: ");
        fflush(stdin);
        scanf ("%c",&c);
    }while (c != 's' && c != 'S');

    return 0;
} 
