#include <stdio.h>
#include <stdlib.h>

void
cambio (int *a, int *b) {

    int aux;

    aux = *a;
    *a = *b;
    *b = aux;
}

int
main (int argc, char *argv[])
 {

     int x = 10;
     int y = 5;

     cambio (&x, &y);

     printf ("El valor de x = %i\n", x);
     printf ("El valor de y = %i\n", y);

     return EXIT_SUCCESS;
 }

