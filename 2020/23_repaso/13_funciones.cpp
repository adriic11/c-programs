#include <stdio.h>
#include <stdlib.h>

int maximo (int a, int b) {
    
    int aux;

    if (a > b) {
        aux = a;
    }else {
        aux = b;
    }

    return aux;
}

int
main (int argc, char *argv[])
{   
    int n1,n2,max;

    printf ("Dime un numero: ");
    scanf ("%i",&n1);

    printf ("Dime otro numero: ");
    scanf ("%i",&n2);

    max = maximo (n1,n2);
    
    printf ("El numero mayor es %i\n",max);
    
    return EXIT_SUCCESS;
}

