#include <stdio.h>
#include <stdlib.h>

// 5! = 1*2*3*4*5
int main (int argc, char *argv[]) 
{
    int x;
    int fact;
    
    fact = 1;

    printf ("Indicame el numero: ");
    scanf ("%i",&x);

    for (int i = 1; i <= x; i++) {
        fact = fact * i;
    }
    
    printf ("Factorial de %i = %i\n", x,fact);

    return 0;

}
