#include <stdio.h>
#include <stdlib.h>

struct notas {
   int ingles;
   int mates;
   int fisica;
};

//notas nota;

void pedir (notas *nota);
void imprimir (notas *nota);

int main (int argc, char *argv[])
{
    struct notas nota;
   
    pedir (&nota);
    imprimir (&nota);


    return 0;
}


void pedir (notas *nota) {

    printf ("Indicame la nota de ingles: ");
    scanf ("%i",&nota->ingles);

    printf ("Indicame la nota de mates: ");
    scanf ("%i",&nota->mates);

    printf ("Indicame la nota de fisica: ");
    scanf ("%i",&nota->fisica);
}

void imprimir (notas *nota) {

    printf ("La nota de ingles es: %i\n",nota->ingles);
    printf ("La nota de mates es: %i\n",nota->mates);
    printf ("La nota de fisica es: %i\n",nota->fisica);
}
