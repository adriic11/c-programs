#include <stdio.h>
#include <stdlib.h>

int factorial (int a) {
    
    int aux;

    aux = 1;

    for (int i = 1; i <= a; i++) {
        aux *= i;
    }

    return aux;
}

int
main (int argc, char *argv[])
{
    int n1,n2,fact1,fact2;

    printf ("Indicame un numero: ");
    scanf ("%i",&n1);

    printf ("Indicame otro numero: ");
    scanf ("%i",&n2);

    fact1 = factorial(n1);
    fact2 = factorial(n2);
    
    printf ("El factorial del primer numero %i = %i\n",n1,fact1);
    printf ("El factorial del segundo numero %i = %i\n",n2,fact2);

    return EXIT_SUCCESS;
}

