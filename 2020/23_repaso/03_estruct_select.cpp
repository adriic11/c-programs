#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    unsigned  x;

    printf ("Indicame un numero: ");
    scanf ("%u",&x);

    if (x % 2 == 0) {
        printf ("El numero es par\n");
    }else {
        printf ("El numero no es par\n");
    }

    return EXIT_SUCCESS;
}

