#include <stdio.h>
#include <stdlib.h>
//Escribe un programa que lea dos numeros y muestre por 
//pantalla todos los numeros comprendidos entre esos dos.

int main (int argc, char *argv[])
{
    int x,y;

    printf ("Indicame un numero: ");
    scanf ("%i",&x);

    do {
        printf ("Indicame un numero mayor al anterior: ");
        scanf ("%i",&y);
    } while (x > y);
    
    for (int i = x+1; i<y; i++) 
        printf ("%i,",i);

    printf ("\n");

    int i = x + 1;
    while (i < y) { 
        printf ("%i,",i);
        i++;
    }
    printf ("\n");

    return 0;
}
