#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    int num,aux,j;
    
    j = 0;

    printf ("Indicame un numero : ");
    scanf ("%i",&num);

    for (int i = 2; i < num; i++) {
        aux = num % i;
        if (aux == 0) {
            j = 1;
        }
    } 
    if (j == 1) {
        printf ("El numero %i no es PRIMO.\n",num);
    } else {
        printf ("El numero %i si es PRIMO.\n",num);
    }

    return EXIT_SUCCESS;
}
