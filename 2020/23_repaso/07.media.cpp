#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    int i,x,y;
    float suma;

    printf ("Indicame cuantos numeros para hacer la media: ");
    scanf ("%i",&x);
    
    i = 0;
    suma = 0;
    while ( i < x) {

        printf ("Indicame el numero %i : ",i+1);
        scanf ("%i",&y);
        
        suma += y;
        i++;
    }
        
     suma = suma / x;
    
    printf ("El resultado de la suma es: %f\n",suma);

    return EXIT_SUCCESS;
}

