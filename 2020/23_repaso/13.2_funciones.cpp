//hallar si dos numeros diferentes son primos con una funcion

#include <stdio.h>
#include <stdlib.h>

int primos (int a) {

    int aux,j;
    j = 0;

    for (int i = 2; i < a && j == 0; i++) {
        aux = a % 2;
    } if (aux == 0) {
        j = 1;
    }

    return j;
}
    

int main (int argc, char *argv[]) 
{
    int n1,n2,prim1,prim2;

    printf ("Indicame el primer numero: ");
    scanf ("%i",&n1);

    printf ("Indicame el segundo numero: ");
    scanf ("%i",&n2);

    prim1 = primos (n1);
    prim2 = primos (n2);
    
    if (prim1 == 0) {
    printf ("El numero %i SI es primo\n",n1);
    } else {
        printf ("El numero %i NO es primo.\n",n1);
    }
    if (prim2 == 0){
    printf ("El numero %i SI es primo.\n",n2);
    } else {
        printf ("El numero %i NO es primo.\n",n2);
    }

    return EXIT_SUCCESS;
}
