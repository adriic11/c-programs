#include <stdio.h>
#include <stdlib.h>

int
suma (int op1, int op2)
{
    return op1 + op2;
}

int
main (int argc, char *argv[])
{
    int a = 2;
    int b = 5;
    int c;

    c = suma (2 * a + 3, b);

    printf ("c = %i\n", c);

    return EXIT_SUCCESS;
}

