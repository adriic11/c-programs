#include <stdio.h>
#include <stdlib.h>

int
suma (int op1, int op2)
{
    return op1 + op2;
}

void
incrementa (int *variable, int cantidad)
{
    *variable += cantidad;
}

int
main (int argc, char *argv[])
{
    int a = 3;
    int b = 5;
    int c;

    c = suma (2 * a + 3, b);

    printf ("c = %i\n", c);

    incrementa (&a, 5);
    printf ("a = %i\n", a);

    return EXIT_SUCCESS;
}

