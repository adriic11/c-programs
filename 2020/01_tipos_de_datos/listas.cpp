#include <stdio.h>
#include <stdlib.h> /*Para la costante EXIT_SUCCES */

#define N 4 /* Variable de prepocesador */
#define ROW 5
#define COL 4


int main () {

    int sueldo0,
        sueldo1,
        sueldo2,
        sueldo3;

    /* N es opcional si hay inicializacion 
     * La inicalizacion de listas solo se puede hacer en la declaracion*/
    int sueldo[N] = { 900, 1900, 2999, 1457 };
    
    int *sueldo_max;  //El tipo de datos seria int*, Estoy declarando un puntero, la diferencia entre sueldo_max y sueldo es que en sueldo_max solo va a ir la direccion de un entero.
      
    /*ROW es opcional si hay inicalizacion */
    int tablero [ROW][COL] = {
        {1, 0, 2, 4}, //Valores de la fila 0
        {1, 0, 2, 4}, //Valores de la fila 1
        {1, 0, 2, 4}, //Valores de la fila 2
        {1, 0, 2, 4}, //Valores de la fila 3
        {1, 0, 2, 4} //Valores de la fila 4
    };

    /*Una vez declarado solo de puede asugnar elemento a elemento */
    sueldo[0] = sueldo[1] = sueldo[2] = sueldo[3] = 1400;
    tablero[0][3] = 5;

    sueldo == &sueldo[0]; // => true
    
    sueldo_max = &sueldo[1]; //Guardo la direccion de sueldo[1]

    printf("%lu\n", sizeof (sueldo)); // => 16 = 4 ints * 4 bytes/int
    printf("%lu\n", sizeof (sueldo_max)); // => 8 => Todas dir son 8 bytes
    
    return EXIT_SUCCESS;
}
