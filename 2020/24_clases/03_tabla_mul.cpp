#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    int num,i;

    printf ("Indicame un numero: ");
    scanf ("%i",&num);

    if (num >=1 && num <= 20) {
        for (i = 1; i <= 10 ; i++)
            printf("%i x %i = %i\n",num,i,num*i);
    }
    return EXIT_SUCCESS;
}

