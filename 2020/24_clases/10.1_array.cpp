//Array que guarde 10 numeros enteros que se lean por teclado.
//Recorrer el array y calcular cuantos numeros son negativos, positivos o ceros.

#include <stdio.h>
#include <stdlib.h>

int main () {

    int a [10];
    int contp,contn,contc;
    
    contp = 0;
    contn = 0;
    contc = 0;

    for (int i = 0; i<10; i++) {
        printf ("Introduce el numero [%i]: ",i);
        scanf ("%i",&a[i]);
    }

    for (int j = 0; j<10;j++) { 
        printf ("El numero [%i] = %i.\n",j,a[j]);
  
        if (a[j] < 0) {
        printf ("El numero es negativo.\n");
        contp ++;
        } else {
        printf ("El numero es positivo.\n");
        contn ++;
        } if (a[j] == 0) {
        printf ("El numero es el 0.\n");
        contc++;
        }
    }
    printf ("Numeros positivos = %i\n",contp);
    printf ("Numeros negativos = %i\n",contn);
    printf ("Numeros 0 = %i\n",contc);

    return 0;
}
