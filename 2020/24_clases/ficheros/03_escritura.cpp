//Escribir un caracter leidos del teclado de un fichero.


#include <stdio.h>
#include <stdlib.h>

int main () {
    
    FILE *f;
    char c;

    f = fopen("Ejemplo.txt","w");

    if (f == NULL) {
        printf ("No se ha podido abrir el archivo.\n");
        exit(1);
    }

    fscanf (f,"%c",&c);
    fprintf (f,"%c",c);

    fclose(f);

    return 0;
}
