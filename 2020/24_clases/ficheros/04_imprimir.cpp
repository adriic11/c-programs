//Imprime un caracter leido de un fichero

#include <stdio.h>
#include <stdlib.h>

int main () 
{
    FILE *f;
    char c;

    f = fopen("Ejemplo.txt","r");

    if (f == NULL) {
        printf ("No se ha podido abrir el archivo.\n");
        exit(1);
    }

    fscanf (f,"%c",&c);

    printf ("%c",c);

    fclose(f);

    return 0;
}
