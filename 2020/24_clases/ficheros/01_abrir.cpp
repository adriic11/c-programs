//Programa para abrir un fichero

#include <stdio.h>
#include <stdlib.h>

int main () 
{
    FILE *f;

    fopen("Ejemplo.txt","r");

    if (f == NULL) {
        printf ("No se ha podido abrir el fichero.\n");
        exit(1);
    }

    fclose(f);

    return 0;
}
