//Programa que lee dos caracteres y los copia en otro archivo

#include <stdio.h>
#include <stdlib.h>

int main ()
{
    FILE *f, *f1;
    char c;

    f = fopen("Ejemplo.txt","r");
    f1 = fopen("Ejemplo2.txt","w");

    if (f == NULL) {
        printf ("No se ha podido abrir el archivo.");
        exit(1);
    }

    if (f1 == NULL) {
        printf ("No se ha podido abrir el archivo.");
        exit(1);
    }

    fscanf (f,"%c",&c);
    fprintf (f1,"%c",c);

    fclose(f);
    fclose(f1);
    return 0;
}
