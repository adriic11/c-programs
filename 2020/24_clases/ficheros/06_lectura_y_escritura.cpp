//Crea un programa que vaya leyendo las frases que el usuario teclee y las guarde en un fichero
//llamado "registroDeUsuario.txt".
//Terminara cuando la frase introducida sea "fin" (esta frase no debera guardarse en el fichero).


#include <stdio.h>
#include <stdlib.h>

int main () {

    FILE *f;
    
    char frase[30];

    f = fopen ("registroDeUsuario.txt","wt");

    while (frase != "fin") {
        printf ("Indicame una frase: ");
        scanf ("%s",frase);

        fgets  (f,"%s",frase);
        fprintf (f,"%s",frase);
    }

    fclose(f);

    return 0;
}
