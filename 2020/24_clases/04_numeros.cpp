#include <stdio.h>
#include <stdlib.h>
//Leer un numero e indicar si es positivo o negativo. El proceso se repetira hasta que se introduzca un 0.

int
main (int argc, char *argv[])
{

    int num;
   
    while (num !=0) {
        printf ("Indicame un numero: ");
        scanf ("%i",&num);

        if(num > 0) {
           printf ("El numero %i es positivo.\n",num);
       }else if (num == 0){
           printf ("\n");
       }else {
           printf ("El numero %i es negativo.\n",num);
       }
    }
    /*do {
        printf ("Indicame un numero: ");
        scanf ("%i",&num);

        if(num >= 0) {
            printf ("El numero %i es positivo.\n",num);
        }else {
            printf ("El numero %i es negativo.\n",num);
        }

    }while (num !=0);
*/
    return EXIT_SUCCESS;
}

