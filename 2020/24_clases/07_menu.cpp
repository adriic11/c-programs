//Menu: 1 - Cubo de un numero, 2 - Par o impar, 3- Salir.

#include <stdio.h>
#include <stdlib.h>

int main ()
{
    int opc,cubo,num,n1,n2;

    printf ("1.-Cubo de un numero.\n");
    printf ("2.-Numero par o impar.\n");
    printf ("3.-Salir.\n");
    
    printf ("Elige la opcion: ");
    scanf ("%i",&opc);

    switch (opc) {
        case 1 : printf ("Indicame el numero: ");
                 scanf ("%i",&num);

                 cubo = num*num*num;

                 printf ("El cubo del numero %i = %i\n",num,cubo);
                 break;

        case 2: printf ("Indicame el primer numero: ");
                scanf ("%i",&n1);

                printf ("Indicame el segundo numero: ");
                scanf ("%i",&n2);

                if (n1 % 2 == 0) {
                    printf ("El numero %i es PAR.\n",n1);
                }else {
                    printf ("El numero %i es IMPAR.\n",n1);
                }
                 if (n2 % 2 == 0 ) {
                    printf ("El numero %i es PAR.\n",n2);
                }else  {
                    printf ("El numero %i es IMPAR.\n",n2);

                }

                break;
        case 3 : printf ("Has salido del programa.\n");
    }

    return 0;
}
