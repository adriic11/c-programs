//Mostrar el promedio de notas de tres cursos.

#include <stdio.h>
#include <stdlib.h>

int main () 
{
    int i;
    int nota [3] = {14,17,11};
    char curso [3] = {'D','E','I'};
    int sum;
    sum = 1;
    int prom;

    printf ("Promedio de cursos.\n");
    printf ("CURSO \t NOTA\n");
    printf ("----------------------\n");

    for (i = 0; i<3;i++) {
        printf ("%c \t %i \n",curso[i],nota[i]); 
        sum += nota[i]; 
        
    }
    prom = sum / 3;
    printf ("Promedio: %i\n",prom);


    return 0;
}
