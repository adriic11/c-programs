//Crear un menu que permita crear un array con el tamaño que el usuario quiera.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 10

int num_primo (int a);

void cont_vocal () {
    
    char word[N];
    int n;
    int i;
    int cont = 0;

    printf ("Indicame el tamaño del array: ");
    scanf ("%i",&n);

    printf ("Indicame la palabra de tamaño maximo %i: ",n);
    scanf ("%s",word);
    
    for (i = 0; i<n; i++){
    if (word[i] == 'a' || word[i] == 'e' || word[i] == 'i' || word[i] == 'o' || word[i] == 'u') {
            cont ++;
        }
    }
    printf ("El numero de vocales es: %i\n",cont);
}

int main () 
{
    char c[N];
    int n[N];
    int opc,num,aux;
    float decimal[N];
    int i,j;
    int primo;  
    
    printf ("1.-Crear un array de caracteres.\n");
    printf ("2.-Crear un array de numeros enteros.\n");
    printf ("3.-Crear un array de numeros decimales.\n");
    printf ("4.-Ordenacion de array de numeros enteros\n");
    printf ("5.-Pedir cadena de caracteres y devolver nº de vocales que tiene.\n");
    printf ("6.-Indicar si un numero es PRIMO o no.\n");

    printf ("Elige la opcion: ");
    scanf ("%i",&opc);

    switch (opc) {
        case 1: printf ("Indicame la longitud del array: ");
                scanf ("%i",&num);

                for (i = 0; i < num; i++) {
                    printf ("Ingresa el caracter: \n");
                    scanf ("%c",&c[i]);
                fflush (stdin);
                }
                    for (i = 0; i < num ; i++){
                        printf ("%c",c[i]);
                    }
                break;

        case 2: printf ("Indicame la longitud del array: ");
                scanf ("%i",&num);

                for (i = 0; i< num ; i++){
                    printf ("Indicame el numero: ");
                    scanf ("%i",&n[i]);
                }
                    for (i = 0; i < num; i++) {
                        printf ("%i ",n[i]);
                    }
                    printf ("\n");
                    break;
       
        case 3: printf ("Indicame la longitud del array: ");
                scanf ("%i",&num);
                
                for (i = 0; i < num; i++) {
                    printf ("Indicame el decimal: ");
                    scanf ("%f",&decimal[i]);
                }

                for (i = 0; i< num; i++) {
                    printf ("%f ",decimal[i]);
                }
                printf ("\n");
                break;

        case 4 : printf ("Indicame la longitud del array: ");
                 scanf ("%i",&num);

                 for (i = 0; i < num; i++) {
                     printf ("Indicame el numero entero: ");
                     scanf ("%i",&n[i]);
                 }

                 for (i = 0; i < num; i++){
                     for (j = i; j < num; j++){
                         if (n[i] > n[j]) {
                            aux = n[i];
                            n[i] = n[j];
                            n[j] = aux;
                         } 
                     }
                 }

                 for (i = 0; i<num; i++) {
                     printf ("%i ",n[i]);
                 } 
                 printf ("\n");
                 break;

        case 5: cont_vocal ();
                break;

        case 6: printf ("Indicame un numero: ");
                scanf ("%i",&num);

                primo = num_primo (num);

                if (primo == 0) {
                    printf ("El numero %i SI es primo.\n",num);
                }else {
                    printf ("El numero %i NO es primo.\n",num);
                }
    }

    return 0;
}

int num_primo (int a) { 
    
    int j,aux;
    j = 0;

    for (int i = 2; i < a && j == 0; i++ ) {
        aux = a % 2;
    } if (aux == 0) {
        j = 1;
    }
     return j;
}
