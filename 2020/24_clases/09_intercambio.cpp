//Pedir dos numeros enteros y decir si son iguales o no y si no lo son intercambiarlos y pintarlos.


#include <stdio.h>
#include <stdlib.h>

int main ()
{
    int n1,n2,aux;

    aux = 1;
    

    printf ("Indicame el primer numero: ");
    scanf ("%i",&n1);

    printf ("Indicame el segundo numero: ");
    scanf ("%i",&n2);

    if (n1 == n2) {
        printf ("Los numeros son iguales.\n");
    } else {
        aux = n1;
        n1 = n2;
        n2 = aux;
         printf ("El primer numero es %i y el segundo es %i\n",n1,n2);
    }

    return EXIT_SUCCESS;
}
