//Realiza un programa que calcule y muestre por la salida estandar los cuadrados de los 10 primeros numeros enteros mayores que cero.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {

    int i;
    for ( i = 1; i <= 10; i++) {
        printf ("%i, \n",i*i);
    }

    return 0;
}
