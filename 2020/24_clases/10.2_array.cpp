//Rellena un array de 10 numeros que se leen por teclado.
//Calcula y muestra la media de los valores positivos y los valores negativos.

#include <stdio.h>
#include <stdlib.h>

int main () {

    int a [10];
    int contp,contn;
    int mediap,median,suma,suma1;
    int j,i;
    
    contp = 0;
    contn = 0;

    suma1= 0;
    suma= 0;

    for (i = 0; i<10; i++) {
        printf ("Introduce el numero [%i]: ",i);
        scanf ("%i",&a[i]);
    }

    for (j = 0; j<10;j++) { 

        if (a[j] < 0) {
        contn++;
        suma += a[j]; 

        } else if (a[j] > 0){
        contp++;
        suma1 += a[j];
        
        
        }
    }

    mediap = suma1 / contp;
    printf ("La media de los numeros positivos = %i\n",mediap);
    median = suma / contn;
    printf ("La media de los numeros negativos = %i\n",median);

    return 0;
}
