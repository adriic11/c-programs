//Ingresar 10 numeros enteros y mostrar los numeros pares.

#include <stdio.h>
#include <stdlib.h>

int main () 
{
    int i;
    int num [10];
    int pares [10];
    int cont = 0;

    for (i = 0; i<10;i++) { 
        printf ("Ingresa el numero [%i]: ",i); 
        scanf ("%i",&num[i]);
        if (num[i] % 2 == 0) {
            pares[cont] = num[i]; 
            cont++;
        }
    }
    for (i = 0; i<cont; i++) {
        printf ("Los numeros pares son: %i\n",pares[i]);    
    }

    return 0;
}
