//Calcular la letra del DNI.


#include <stdio.h>
#include <stdlib.h>

int main () {
    
    int dni,l;
    char letras [23] = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};

    printf ("Indicame tu DNI: ");
    scanf ("%i",&dni);

    l = dni % 23;

    printf ("La letra de tu DNI es: %c\n", letras[l]);

    return 0;

}
