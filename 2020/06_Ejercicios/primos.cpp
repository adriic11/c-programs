#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
  
  /*DECLARACION DE VARIABLE */
    int posible_primo;  

  /*ENTRADA Y SALIDA DE DATOS */
  //Una alternativa a printf y scanf es usar la familia
  //de funciones put(printf) y get(scanf) pero estan pensadas para 
  //trabajar con caracteres o cadenas (de caracteres)
  printf("Indicame un numero: ");
  scanf("%i",&posible_primo); 

  /* %i es un especificador de formato
   * Hay mas especificadsores de formato
   * man scanf -> para ver el manual
   * & -> Nos indica la posicion 
   */

  /* SALIDA DE DATOS */
  printf("Has introducido el numero %i\n", posible_primo);

  /*Para ver el dibujo de la salida de datos, por ejemplo si ponemos
   * 7 en posible primo sera necesario mandar el codigo ASCII de ese 
   * dibujo. Luego %i va a mandar a pantalla un 55 en hexadecimal
   * 0x37
   */

  return EXIT_SUCCESS; 
  /*EXIT_SUCCESS es una constante definida 
   * en stdlib.h y vale 0
   */

}
