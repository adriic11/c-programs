#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
  int lado;

  printf ("¿Cuántos lados quiere? \n");
  scanf ( "%i", &lado);
  
  for (int i; i>lado; i++){
      for (int j; j>lado; j++)
      printf("*");
      printf("\n");
  }

    return EXIT_SUCCESS;
}

