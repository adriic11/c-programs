#include <stdio.h>
#include <stdlib.h>

#define MENU "				\n\
					\n\
	 **********************		\n\
	/ MENU DE OPERACIONES /		\n\
	**********************		\n\
					\n\
	Tus opciones son:		\n\
	1.Sumar dos numeros.		\n\
	2.Restar dos numeros.		\n\
	3.Multiplicar dos numeros.	\n\
	4.Dividir dos numeros.		\n\
					\n\
	Escribe tu opcion:	      	\n\
"

enum TOperacion { sumar, restar, multiplicar, dividir };

int main (int argc, char *argv[]) {
    int opcion;
    int op1, op2;
    int suma, resta, multiplicacion, division;
	
	 do {
        printf (MENU);
        scanf ("%i", &opcion);
        opcion--;
    } while (opcion > dividir);
	

    printf ("Has elegido: ");
    switch (opcion) {
        case sumar:
		printf("Sumar dos numeros.\n");
		printf("Introduzca el primer numero: \n");
		scanf("%d", &op1);
		printf("Introduzca el segundo numero: \n");
		scanf("%d", &op2);

		suma = op1 + op2;
            printf("La suma es: %i\n", suma);
            break;
        case restar:
	    	printf("Restar dos numeros.\n");
	    	printf("Introduzca el primer numero: \n");
                scanf("%d", &op1);
                printf("Introduzca el segundo numero: \n");
                scanf("%d", &op2);

		resta = op1 - op2;
            printf("La resta es: %d\n", resta);
            break;
	case multiplicar:
	    	printf("Multiplicar dos numeros,\n");
		printf("Introduzca el primer numero: \n");
                scanf("%d", &op1);
                printf("Introduzca el segundo numero: \n");
                scanf("%d", &op2);

		multiplicacion = op1 * op2;
		printf("La multiplicacion es: %d", multiplicacion);
		printf("\n");
		break;
	case dividir:
		printf("Dividir dos numeros.\n");
		printf("Introduzca el primer numero: \n");
                scanf("%d", &op1);
                printf("Introduzca el segundo numero: \n");
                scanf("%d", &op2);

		division = op1 / op2;
		printf("La division es: %d\n", division );
		break;
    }

    return EXIT_SUCCESS;
}

 


