#include <stdio.h>
#include <stdlib.h>

int main () {
  
  int i,num;
  int suma=0;
  int mul=0;

  printf("Introduzca un numero: ");
  scanf("%d", &num);

  for(i=1; i<num; i++) {
      mul=num%i;
      if(mul==0) {
          suma+=1;
      }
  }
  if(suma==num) {
      printf("El numero %d es perfecto.\n",num);
  }
  else
      printf("El numero %d no es perfecto.\n",num);
  
  return EXIT_SUCCESS;

}
