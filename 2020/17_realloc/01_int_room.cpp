#include <stdio.h>
#include <stdlib.h>

int
usuario_quiera (int s, int *room)
{
    return room[s - 1] < 0 ? 0 :1;  //Devuelve un 0 o 1, si es < 0 devuelve 0 si es > devuelve 1

}
int
main (int argc, char *argv[])
{
    /* DECLARACION DE VARIABLES */
    int *room;
    int summit = 0;

    room = (int *) malloc (sizeof (int));

    /* ENTRADA DE DATOS */
    do {
        printf ("Número: ");
        scanf ("%i", &room[summit++]);
        room = (int *) realloc ( room, (summit +1) * sizeof (int) );
    } while (usuario_quiera (summit, room));
    summit--;

    /* SALIDA DE DATOS */
    printf ("\n");
    for (int i=0; i<summit; i++)
        printf ("%i: %i\n", i+1, room[i]);
    printf ("\n");

    free (room);

    return EXIT_SUCCESS;
}

