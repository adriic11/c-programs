#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <string.h>

#define N 0x20

int                                   
main (int argc, char *argv[]) 
{ 
  unsigned repeticiones = 0;
  char nombre[N];                                     /*nombre es una variable de tamaño N y de tipo caracter,
                                                      es una variable que contiene la direccion de memoria del primer char*/

  do {
      __fpurge (stdin);
      printf("Repeticiones: ");
  } while (!scanf (" %u", &repeticiones));

  printf ("Dime tu nombre: ");
  scanf (" %s", nombre);
 
  /*BUCLE*/
  for (unsigned i=0; i<repeticiones; i++)  
      printf("Tu nombre: %s\n", nombre);

  for (unsigned i=0; i<strlen(nombre); i++)
                                                      //strlen -> devuelve la cantidad de letras
                                                      // #include <string.h>
  return EXIT_SUCCESS;

}
