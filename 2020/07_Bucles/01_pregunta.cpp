#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x20

int
main (int argc, char *argv[])
{
    char palabra[N];

    printf ("Palabra: ");
    scanf (" %[abcl]", palabra);
    /*
     * Conjunto de selección limitado a 4 letras.
     */

    printf ("Palabra: ");
    scanf (" %4[abcl]", palabra);
    /*
     * Conjunto de selección con rango.
     */

    printf ("Número hexadecimal: ");
    scanf (" %[0-9a-fA-F]", palabra);
    
    printf ("Palabra: ");
    scanf (" %[!0-9]");
    /*
     * Conjunto de selección inverso, todo menos números.
     */

    printf ("Palabra: ");
    scanf (" %[!\n]", palabra);
    /*
     * Línea entera => Todo hasta el \n
     */
    
    /* Sacar un guión o una barra sin asignar
    scanf (" %*[-\]"); El asterisco se usa para que no lo asigne a ninguna variable
    */


    printf("%s\n", palabra);

    return EXIT_SUCCESS;
}

