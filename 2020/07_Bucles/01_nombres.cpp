#include <stdio.h>
#include <stdlib.h>

//indent 01_nombres.cpp --> Te estrucutura el programa:

int                                   
main (int argc, char *argv[]) 
{ 
  /*DECLARACION DE LA VARIABLE*/  
  char nombre[15];
  int num;

  printf("Escribeme tu nombre: \n");                    //Cuando en un scanf ponemos un & en una variable se denomina paso por referencia
  /*%s-> Especificador de formato*/                     //
  scanf("%s",nombre);                                   

  /*BUCLE*/
  printf("¿Cuantas veces quieres repetir tu nombre? \n");
  scanf("%i",&num);
  for (int i=0; i<num; i++) {                        //Para i=0, es decir, empezando el bucle en 0, i se va a repetir hasta que 0 llegue a 10 
      printf("Mi nombre es: %s.\n",nombre);         //la i se irá incrementando en 1 hasta llegar al límite que hemos puesto, en este caso 10 
  }

  return EXIT_SUCCESS;

}
