#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


void *funcion (void *dato)
{
	printf ("Soy un hilo.\n");
}

int main()
{
	int dato=0;
	pthread_t id_hilo[3];
	//id_hilo -> identificador del hilo
	//NULL atributos del hilo, en el caso de NULL por defecto
	//funcion -> la funcion que realiza el hilo
	//dato, los datos que pasamos a la funcion
	for (int i=0; i<3; i++)
	{
		pthread_create(&id_hilo[i], NULL, funcion, (void*)&dato);
	}

	for (int i=0; i<3; i++)
	{
		pthread_join(id_hilo[i], NULL);
	}
	return 0;
}
