#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void * imprimir (void *dato)
{
	printf ("Hhola.\n");
	pthread_exit(NULL);
}

int main()
{
	pthread_t hilo[5];
	int dato;
	for (int i=0; i<5;i++)
	{
		pthread_create(&hilo[i],NULL,imprimir,(void*)&dato);
	}

	for (int i=0; i<5; i++)
	{
		pthread_join(hilo[i],NULL);
	}

	return 0;
}
