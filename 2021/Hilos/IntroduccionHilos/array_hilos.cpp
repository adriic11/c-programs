#include <pthread.h>
#include <stdio.h>


void * imprimir (void *puntero)
{
	char *mensaje;
	mensaje = (char*)puntero;
	printf("%s",mensaje);
	pthread_exit(NULL);
	
	return puntero;
}


int main ()
{
	pthread_t hilo[5];
	char *mensaje = "Hola";

	for (int i=0; i<5; i++)
	{
		pthread_create(&hilo[i],NULL,imprimir,(void *)mensaje);
		pthread_join (hilo[i], NULL);		
	}

	return 0;
}
