#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

int hucha = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


void *imprimir (void* i)
{
	printf ("Hhola.\n");
	int*algo = (int*)i;
	pthread_mutex_lock(&mutex);
	hucha++;
	printf("Soy el hilo %i y el valor de la hucha %i.\n",*algo,hucha);
	pthread_mutex_unlock(&mutex);
	pthread_exit(NULL);
}

int main()
{
	int dato = 10;
	pthread_t hilo[5];
	for (int i=0; i<5;i++)
	{
		pthread_create(&hilo[i],NULL,imprimir,(void*)&dato);
		pthread_join(hilo[i],NULL);
	}


	printf ("Ahorros : %i.\n", hucha);
	return 0;
}
