//Crea un programa dónde el hijo escriba el mensaje HOLA SOY EL HIJO y que el padre escriba HOLA SOY EL PADRE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

 int main (void) {

     pid_t pid;

     pid = fork();

     printf ("Se inicia el proceso %d\n", getpid());
     if (pid == 0 ) {
         printf ("HOLA SOY EL HIJO %d\n", getpid());
     } else if (pid > 0) {
         printf ("HOLA SOY EL PADRE %d\n", getpid());

     }

     return 0;
}

