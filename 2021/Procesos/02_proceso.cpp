//Crea un programa dónde el padre cree un programa huérfano. 
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main (void) {

    pid_t pid;
    
    pid = fork();

    printf ("Se inicia el proceso padre : %d\n", getpid());
    if (pid > 0 ) {
        printf ("Soy el proceso padre %d\n", getpid());
    } else if (pid == 0) {
        printf ("Soy el proceso hijo %d\n", getpid());
        sleep(1);                                                                       //Duerme al proceso padre para crear un hijo huerfano

        printf ("El proceso queda huerfano, PID=%d, PPID=%d\n", getpid(), getppid());   //Para verificar que es el init mostramos el ppid
       // exit();
    }

    return 0;
}
