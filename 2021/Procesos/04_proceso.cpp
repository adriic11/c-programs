 #include <stdlib.h>
  #include <stdio.h>
  #include <unistd.h>
 
  int main (void) {
 
      pid_t pid;
 
      pid = fork();
 
      printf ("Se inicia el proceso padre : %d\n", getpid());
      if (pid == 0 ) {
          printf ("HOLA SOY EL HIJO %d\n", getpid());
         // printf ("%d",system("ps lf"));
      } else if (pid > 0) {
          printf ("HOLA SOY EL PADRE %d\n", getpid());
 
      }
 
      return 0;
 }

