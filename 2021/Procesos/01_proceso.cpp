//Crea un programa que cree un proceso hijo y que muestre el pid del padre y del hijo y el uid.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (void) {

    printf ("Este es el proceso padre %d. El usuario real es %d\n", getpid(),getuid()); 
    fork();                                                                               //Crea un nuevo proceso, la copia del padre
    printf("Este es el proceso hijo %d. El usuario real es %d\n",getpid(),getuid());     //Proceso del hijo. Tambien es ejecutado por el padre

}
