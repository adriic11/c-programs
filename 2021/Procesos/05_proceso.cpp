//Añade al programa anterior la información de los procesos con la función system.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main () {
	
	pid_t pid;
	pid = fork();

	printf ("Inicio del proceso %d\n",getpid());
	if (pid > 0) {
		printf ("HOLA SOY EL PADRE-> %d\n", getpid());
		//Lista la información sobre todos los procesos más frecuentemente solicitados 
		printf ("%d",system("ps -ax"));                          
	} else if (pid == 0) {
		printf ("HOLA SOY EL HIJO-> %d\n", getpid());
		printf ("%d",system("ps -ax"));
	}

	return 0;
}
