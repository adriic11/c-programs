/*
 * El hijo escribe al padre una nota
 * y la multiplica por 10
*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define SIZE 521

int main ()
{
	int nota;
	int tuberia[2];
	int readbyte;
	pid_t pid;
	char buffer[SIZE];


	pipe(tuberia);

	pid = fork();

	if (pid == 0)
	{
		//Cerramos el lado que no usamos, la de escritura
		close(tuberia[1]);

		while (readbyte = read(tuberia[0],buffer,strlen(buffer))>0)
		{
			nota = atoi(buffer);
			nota = nota * 10;
			
			printf ("%d\n", nota);
		}
	}
	else
	{
		//Cerramos la tuberia que no usamos, la de lectura
		close(tuberia[0]);
		printf ("Indicame la nota: ");
		scanf ("%d",&nota);
		//buffer = itoa(nota);
		sprintf(buffer,"%d",nota);
		write (tuberia[1], buffer, strlen(buffer));
	}
		
}
