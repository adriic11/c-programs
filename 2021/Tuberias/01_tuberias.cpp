/*
Dos procesos(padre e hijo).
Cuando el padre pulse 'a' envie mensaje "hola" al hijo
Cuando el hijo pulse b envie "adios" al padre
*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>


int main ()
{
	char letra[2];
	int tuberia[2];
	int readbyte;
	pid_t pid;
	size_t nbytes;

	nbytes = sizeof(letra);

	pipe(tuberia);

	pid = fork();

	if (pid == 0)
	{
		//Cerramos el lado que no usamos, la de escritura
		close(tuberia[1]);
		printf ("Estoy en el hijo.\n");
		while (readbyte = read(tuberia[0],letra, nbytes)>0)
		{
			switch (letra[0])
				{
					case 'a' : printf("Hola.\n");i
						break;
					case 'b' : printf ("Adios.\n");
						break;
				}
		}
	}
	else
	{
		//Cerramos la tuberia que no usamos, la de lectura
		close(tuberia[0]);
		letra[0] = 'a';
		letra[1] = '\0';
		write (tuberia[1], letra, nbytes);
	}
}
