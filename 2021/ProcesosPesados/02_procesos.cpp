#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int tubo[2];

int main (int argc, char *argv[])
{
	int pid, idHijo, estado;
	
	pipe(tubo);
	pid = fork();	
	
	if (fork() != 0)
	{	
		//El padre ejecuta el comando ls
		dup2 (tubo[1],1);  /*Salida estandar => tubo[1]*/
		close(tubo[0]);  /*Cerramos extremo lectura */
		execl ("/bin/ls","ls", "-l", NULL);
	}
	else 
	{
		/*El Hijo ejecuta el comando wx */
		dup2 (tubo[0],0); 	/*Entrada estrandar => tubo[0] */
		close (tubo[1]); 	/*Cerramos extremo escritura */
		execl ("/usr/bin/wc", NULL);
	}

	return 0;
}
