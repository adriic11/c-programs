#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (void)
{
	int pid, miAbuelo, miPadre, tubo[2];

	pipe(tubo);
	pid = fork();

	if (pid == 0)
	{
		read (tubo[0], &miAbuelo, sizeof(miAbuelo));
		printf ("[H] miPadre = %5d Yo = %5d miAbuelo = %5d\n",
				getppid(), getpid(), pid);
	}
	else 
	{
		miPadre = getppid();
		write (tubo[1], &miPadre, sizeof(miAbuelo));
		printf ("[P] miPadre = %5d Yo = %5d miHijo = %5d\n",
				getppid(), getpid(), pid);
	}
}
