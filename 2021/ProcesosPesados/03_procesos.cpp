#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

void hijo (void)
{
	int i = 1;

	do {
		printf ("%c", 'H');
		if ((i%60)== 0) printf ("\n"));
	}
	while (i++ < 10000000)
		printf ("Fin del proceso hijo.\n");
	_exit(3);
}

int main (void)
{
	int pid, resultado, estado;
	char ch;

	pid = fork();

	if (pid == 0)
	{
		hijo();
	}
	else 
	{
		scanf ("%c", ch);
		resultado = kill (pid, SIGKILL);ç
		printf ("Enviado SIGKILL al hijo con resultado %d\n", resultado);
		resultado = wait(&estado);
		if (_LOW(estado) > 0)
			printf ("Abortado %d con señal %d\n",
					resultado, _LOW (estado));
		else
			printf ("Terminado hijo %d con estado %dn",
					resultado, _HIGH (estado));

	}
}
