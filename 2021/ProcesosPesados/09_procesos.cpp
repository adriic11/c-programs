/*
Escribe un programa:
Que genere un proceso hijo, el cual espera hasta que reciba una señal de SIGINT, tras lo cual se muere.
El proceso padre debe esperar a que muera el proceso hijo anterior y crear otro hijo
Todo este proceso se crea en un bucle infinito. 
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int condicion = 1;

void handle1 (int sig)
{
	printf ("SEÑAL SIGINT RECIBIDA.\n");
	kill (getppid(),SIGINT);
}

void handle2 (int sig)
{
	printf ("SEÑAL RECIBIDA.\n");
}

int main (void)
{
	pid_t pid;
	
	for (;;)
	{
		printf("Padre: creando hijo.\n");
		pid = fork();

	if (pid > 0)
	{
		printf ("Proceso padre: %d\n",getpid());
		signal (SIGUSR1,handle1);
		signal (SIGCHLD,handle2);
	} else if (pid == 0)
	{	
		printf ("Proceso hijo esperando: %d\n",getpid());
		signal ()
	}
	else
		printf ("SE HA PRODUCIDO UN ERROR.\n");

	while (condicion);

	return 0;
}
