/*
Implementar un programa en C en el que un proceso padre crea un proceso hijo. 
El padre, transcurridos 10 segundos después de la creación del hijo, 
debe enviar una señal SIGUSR1 al proceso hijo, y a continuación terminación su ejecución. 
El proceso hijo, cuando reciba la señal, debe mostrar el siguiente mensaje por pantalla: “SEÑAL RECIBIDA”  
y a continuación debe terminar su ejecución. Realiza este ejercicio con signaction. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

struct sigaction act;

void handle_signal (int sig)
{
	printf ("SEÑAL RECIBIDA.\n");
}

int main (void)
{
	pid_t pid;
	
	pid = fork();

	if (pid < 0)
	{
		//Proceso padre
		printf ("Proceso Padre = %d\n",getpid());
		sleep(10);
		kill(pid,SIGUSR1);
		exit(0);
	}
	else if (pid == 0)
	{
		//Proceso hijo
		act.sa_handler = handle_signal;
		printf ("Proceso Hijo = %d\n",getpid());
		sigaction (SIGUSR1, &act, NULL);
		pause();
	}
	else
	{
		printf ("Ha ocurrido un ERROR\n");
	}

	return 0;
}
