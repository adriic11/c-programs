//Implementar un programa que espere 10 segundos a que el usuario pulse una tecla. 
//En caso de pasar los tres segundos sin pulsar nada, el programa termina sin seguir esperando.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int condicion = 1;

void handle_signal (int sig)
{
	printf ("SEÑAL RECIBIDA.\n");
	raise (SIGINT);
}

void handle_sigint (int sig)
{
	printf ("Se termina el programa.\n");
	condicion = 0;
}

int main (void)
{
	signal (SIGALRM,handle_signal);
	signal (SIGINT, handle_sigint);

	printf ("Esperando 10 segundos...\n");
	alarm(10);

	while (condicion);

	return 0;
}
