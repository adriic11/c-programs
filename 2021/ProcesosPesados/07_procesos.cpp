/*Implementar un programa en el que el proceso padre cree un hijo. Se debe cumplir lo siguiente:
     ◦ El padre debe capturar la señal SIGUSR1 y la señal SIGCHLD. Se debe un tratamiento diferente para cada señal. 
     ◦ El hijo se duerme 2 segundos, envía la señal SIGUSR1 al padre y se duerme 3 segundos antes de terminar
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void handle1 (int sig)
{
	printf ("SEÑAL USR1 RECIBIDA.\n");
}

void handle2 (int sig)
{
	printf ("SEÑAL SIGCHL RECIBIDA.\n");
}

int main (void)
{
	pid_t pid;

	pid = fork();

	if (pid > 0)
	{
		printf ("Proceso padre: %d\n",getpid());
		signal (SIGUSR1,handle1);
		signal (SIGCHLD,handle2);
	} else if (pid == 0)
	{
		printf ("Proceso hijo: %d\n",getpid());
		sleep(2);
		kill (getpid(),SIGUSR1);
		sleep(3);
	}
	else
		printf ("SE HA PRODUCIDO UN ERROR.\n");

	return 0;
}
