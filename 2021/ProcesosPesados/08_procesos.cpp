/*Implementar un programa que escriba los número enteros del 1 al 10 en pantalla 
 * de tal forma que los número pares los escribe el padre y los impares el hijo. 
 *  La sincronización es necesaria para que los números parezcan en el orden correcto
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int i;

void handle1 (int sig)
{
	for (i = 1; i<=10; i++) {
		if (i %2 == 0)
			printf ("Numero par: %d\n",i);
	}
	kill(pid,SIGUSR2);
}

void handle2 (int sig)
{
	for (i = 1; i<=10; i++) {
		if (i % 2 != 0)
			printf ("Número impar: %d\n.",i);
	}
}

int main (void)
{
	pid_t pid;

	pid = fork();

	if (pid > 0)
	{
		printf ("Proceso padre: %d\n",getpid());
		signal (SIGUSR1,handle1);
		pause();
	} 
	else if (pid == 0)
	{
		printf ("Proceso hijo: %d\n",getpid());
		signal (SIGUSR1,handle2);
	}
	else
		printf ("SE HA PRODUCIDO UN ERROR.\n");

	return 0;
}
