//Implementar un programa que espere 10 segundos a que el usuario pulse una tecla. 
//En caso de pasar los tres segundos sin pulsar nada, el programa termina sin seguir esperando.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


void handle1 (int sig)
{
	printf ("SEÑAL 1 RECIBIDA.\n");
	raise (SIGUSR2);
}

void handle2 (int sig)
{
	printf ("SEÑAL 2 RECIBIDA.\n");
}

int main (void)
{
	signal (SIGUSR1,handle1);
	signal (SIGUSR2, handle2);

	raise (SIGUSR1);

	return 0;
}
