//Realiza un programa que muestre la prioridadde proceso actual

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/resource.h>

int main (void)
{
	pid_t pid;
	int which = PRIO_PROCESS;
	int prioridad;

	pid = getpid();
	prioridad =getpriority(which, pid);

	printf ("Proceso actual: %d\n",pid);
	printf ("Prioridad actual: %d\n",prioridad);

	return 0;



}
