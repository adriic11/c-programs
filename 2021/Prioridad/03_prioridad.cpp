//Crea un proceso huérfano modificando la prioridad de los procesos.
//El padre se ejecuta antes

#include <stdio.h>
#include <unistd.h>
#include <sys/resource.h>

int main (void)
{
	pid_t pid;
	int which=PRIO_PROCESS;
	int prioridad;
	int n_prioridad=10;
	int d;

	//Creamos un hijo
	pid = fork();

	if (pid==0){
		//Proceso hijo huerfano
		prioridad = getpriority(which,getpid());
		printf("Mostramos la prioridad del proceso hijo %d\n",prioridad);
		//Bajamos la prioridad del proceso hijo
		d = setpriority(which,pid,n_prioridad);
        }  if (d==-1) {
			printf("ERROR\n");	
		}else {
			prioridad = getpriority(which,getpid());
			printf("Mostramos la prioridad del proceso padre %d\n", prioridad);
			printf("El pid de mi padre es %d\n",getpid());
		}
	/*else {
		prioridad = getpriority(wich,getpid());
		printf("Mostamos la prioridad del proceso padre %d\n",prioridad);
		d=setpriority(wich,pid,n_prioridad);
		if (d==-1) {
			printf("ERROR\n");
		}else {
			prioridad = getpriority(wich,pid);
			printf("Mostramos la nueva prioridad del proceso padre %d\n", prioridad);
			system("ps -ax");
		}*/

		
	return 0;
}
