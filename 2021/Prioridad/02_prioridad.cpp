//Realiza un programa que baje la prioridad de un programa del proceso actual

#include <stdlib.h>
#include <sys/resource.h>
#include <stdio.h>
#include <unistd.h>

int main (void)
{
	pid_t pid;
	int which = PRIO_PROCESS;
	int g_prio;
	int s_prio;
	
	pid = getpid();
	g_prio = getpriority (which,pid);
  	printf ("Prioridad del proceso actual: %d\n", g_prio);
	
	
	s_prio = setpriority (which,pid,--g_prio);
	g_prio = getpriority (which,pid);

	printf ("Cambiando la prioridad del proceso: %d\n", s_prio);

}
