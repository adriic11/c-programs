#include <signal.h>
#include <stdio.h>
#include <unistd.h>

//Variable global
int condicion = 1;

void manejador ()
{
	printf ("Recibo una alarma.\n");
	condicion = 0;
}

int main ()
{
	//Utilizamos la señal alarm
	//Capturamos la señal
	signal(SIGALRM,manejador);

	printf ("Se va a crear una alarma en 15 segundos.\n");
	//Se crea la alarma, no hace falta especificar la unidad
	alarm(10);

	while(condicion);

	return 0;
}
