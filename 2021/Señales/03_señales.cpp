#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

int main ()
{
    int stat;
    pid_t pid;

    if ((pid = fork()) == 0)
        while(1);
    else
    {
        kill(pid,SIGINT);
        wait(&stat);
        if(WIFSIGNALED(stat))            
		//Muestra el mensaje recibido del hijo, muestra la señal recibida
    		psignal(WTERMSIG(stat), "Child term due to");
		printf("Stat despues de wait %d\n",stat);
    }
}
