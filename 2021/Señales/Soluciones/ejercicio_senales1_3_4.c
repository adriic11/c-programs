#include<stdio.h>
#include<signal.h>
#include<sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int val = 10;
void handler(int sig)
{
    //Incrementa 5
    val += 5;
}
int main()
{
    pid_t pid;
    //SIGNCHILD es la señal enviada a un proceso cuando uno de sus hijos termian
    signal(SIGCHLD, handler);

    // La llamada fork en el caso del padre devuelve el pid del hijo

    if ((pid = fork()) == 0)
    {
        // Si es el hijo, decrementa en 3

        val -= 3;
        printf("Hola soy el hijo %d con pid %d \n",val,getpid());
        exit(0);
    }
    //Suspende la ejecución del proceso en curso hasta 
    //que un hijo especificado por el argumento pid ha terminado
    //o hasta que se produce una señal cuya acción es finalizar el proceso actual 
    //o llamar a la función manejadora
    // waitpid(pid, status, opciones)
    printf("El pid es %d \n",pid);
    waitpid(pid, NULL, 0);
    printf("val = %d\n", val);
    exit(0);
}
