#include <stdio.h>
#include <signal.h>
// Falta una libreria
#include <unistd.h>
#include <time.h>

int main(){
	// la funcion handle_signint no está definida en ninguna parte
	signal(SIGINT, handle_sigint);
	// Podemos escoger dos opciones sin definir una funcion para manejar la señal

	/*
	// Realiza la función del nucleo por defecto esta definida en signal.h
	signal(SIGINT,SIG_DFL); // señal por defecto
	// Ignora la señal por defecto
	signal(SIGINT, SIG_IGN);
	*/


	while(1)
	{
		printf("hello world \n");
		sleep(1);
	}
	return 0;
}
