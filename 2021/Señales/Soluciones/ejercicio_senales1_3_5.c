#include<stdio.h>
#include <stdlib.h>
#include<sys/wait.h>
#include<signal.h>
#include <unistd.h>

pid_t pid;
int counter = 0;
void handler1(int sig)
{
    printf("El primera función manejadora, es el proceso con a %d y pid del hijo %d\n",getpid(),pid);
    counter++;
    printf("counter = %d\n", counter);
    /* Flushes the printed string to stdout */
    fflush(stdout);
    // se la manda a si mismo
    kill(pid, SIGUSR1);
}
void handler2(int sig)
{
    counter += 3;
    printf("El segunda función manejadora, tiene un pid a %d \n",getpid());
    printf("counter = %d\n", counter);
    exit(0);
}
  
int main()
{
    pid_t p;
    int status;
    // SIGGUSR señal definida por el usuario

    signal(SIGUSR1, handler1);
    if ((pid = fork()) == 0)
    {

        // Soy el hijo, y manejo la señal
        printf("Soy el hijo y mando una señal al padre SIGUSR1, con pid %d \n",getpid());
        signal(SIGUSR1, handler2);
        // Mandamos una señal al padre
        kill(getppid(), SIGUSR1);
        //Bucle infinito
	while(1) ;
    }
    if ((p = wait(&status)) > 0)
    {
        counter += 4;
        printf("counter 3 = %d\n", counter);
    }
}
