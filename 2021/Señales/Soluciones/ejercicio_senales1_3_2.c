#include <stdio.h>
#include <signal.h>

// Handle for SIGINT, caused by
// Ctrl-C at keyboard

void handle_sigint (int sig)
{
	//Entra en esta función cuando recive CONTROL+C
	printf("Caught signal %d\n",sig);

	}

	int main(){
		
		signal(SIGINT, handle_sigint);
		//Bucle infinito, vuelve cuando a manejado la señal
		while(1) ;
		return 0;
	}