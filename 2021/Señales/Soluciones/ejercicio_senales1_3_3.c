
#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>
#include<signal.h>
int main()
{
    int stat;
    pid_t pid;

    //realiza una llamada a fork y cuando es el hijo realiza un bucle infinito
    if ((pid = fork()) == 0)
        while(1) ;
    else
    {
    	// Manda una señal a un proceso o a un grupo de proceso kill (pid proceso,n_señal)
        kill(pid, SIGINT);
        wait(&stat);
        //WIFSIGNALED devuelve 0 si el hijo recibe una señal que no sabe manejarla
        // WIFSIGNALED verdadero y devuelve el número que ha terminado el procesp hijo
        if (WIFSIGNALED(stat))

        	// Muestra un mensaje en stderr de la señal recibida
            psignal(WTERMSIG(stat), "Child term due to");
    }
}