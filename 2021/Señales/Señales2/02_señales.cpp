/*
 *
Realiza un programa que cuando reciba control que haga distintas cosas en función de la señal que reciba. 
El programa tiene una variable que se llama i y está inicializada a 20. 
-SIGNINT: Muestra el código del pid del proceso y decrementa la variable i en 1
-SIGQUIT: Muestra el total de la variable i y se incrementa 3
-SIGTSTP: Muestra el toral de la variable i y se incrementa 5
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

//Variables globales
int condicion = 1;
int var = 20;

//Manejador señal stop
void handle_sigtstp (int sig)
{
     printf ("Señal: %d\n",sig);
     printf ("Variable total = %d\n",var);
     printf ("Variable %d incrementada en 5 = %d\n",var,var+=5);
     condicion = 0;

}

//Manejador señal quit
void handle_sigquit (int sig)
{
    printf ("Señal: %d\n",sig);
    printf ("Variable total = %d\n",var);
    printf ("Variable %d incrementada en 3 = %d\n",var,var+=3);
    condicion = 0;
}

//Manejador señal 
void handle_sigint (int sig)
{
    printf ("Señal: %d\n",sig);
    printf ("%d",system("ps"));
    printf ("Variable %d incrementada en 1= %d\n",var,var++);
    condicion = 0;
}

int main ()
{
    int opc;
    //pid_t pid;
    
    //Menu de opciones
    printf ("1.- SIGINT:  Muestra el codigo del pid del proceso y decrementa la variable en 1.\n" 
            "2.- SIGQUIT: Muestra el total de la variable e incrementa 3.\n"
            "3.- SIGTSTP:  Muestra el total de la variable e incrementa a 5.\n");

    printf ("Indicame la opción: \n");
    scanf ("%d",&opc);

    switch (opc) {
        
        case 1:
		printf ("Opción 1: Presione Ctrl+C.\n");
                signal (SIGINT,handle_sigint);
			
           	 while (condicion)
			sleep(20);    
           	 break;

        case 2:
		printf ("Opcion 2: Presione Ctrl+AltGr + \\. \n");	
        	signal (SIGQUIT, handle_sigquit);
			
		while (condicion)
			sleep(10);
                break;

        case 3:
		printf ("Opcion 3: Presiona Ctrl+Z.\n");
		signal (SIGTSTP, handle_sigtstp);
				
                while (condicion) 
			sleep(10);

                break;

	default :
		printf("Solo opciones del 1 al 3.\n");
    }    

    return 0; 

}
