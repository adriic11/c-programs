//Crea un programa que maneje una alarma y que la función que maneja la alarma mande una señal SIGINT a ese proceso (utiliza raise)
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

int condicion = 1;

void handle_alarm (int sig)
{
	printf ("Señal ALARM recibida con proceso %d\n",getpid());
	raise (SIGINT);
}

void handle_sigint (int sig)
{
	printf ("Señal con SIGINT %d\n",sig);
	condicion = 0;
}

int main ()
{
	signal (SIGALRM, handle_alarm);	
	signal (SIGINT, handle_sigint);
	printf ("Alarma puesta en 3 segundos...\n");
	alarm(3);
	
	while (condicion); 

	return 0;
}	
