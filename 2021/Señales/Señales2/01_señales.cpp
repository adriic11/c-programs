//Realiza un proceso que se quede esperando contando los segundos hasta que la persona presione CONTROL+C. 

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

int condicion = 1;

void handle_sigint (int sig)
{
	printf ("Señal: %d\n",sig);
	printf("Fin del contador.\n");

	condicion = 0;
}

int main ()
{
    int cont = 0;
     
    signal (SIGINT,handle_sigint);

    while (condicion) {
        printf ("%d\n",cont);
        cont++;
        sleep(1);
    }
    return 0;

} 
