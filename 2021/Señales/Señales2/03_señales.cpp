//Realiza un programa que le envíesuna señal de SIGINT y cuando maneje esa señal 
//se mande una señal a si mismo con la señal SIGUSR1 y que imprima un mensaje del código de su pid

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

void handle_sigusr1 (int sig)
{
	printf ("SIGUSR1: Está manejando la señal del proceso %d\n", getpid());
}

void handle_sigint (int sig)
{
	printf ("Señal: %d\n",sig);
	printf("SIGINT: Esta manejando la señal del proceso %d\n", getpid());
	kill(getpid(),SIGUSR1);
	
}

int main ()
{
	signal (SIGINT, handle_sigint);
	signal (SIGUSR1, handle_sigusr1);
	
	while (1); 

	return 0;
}	
