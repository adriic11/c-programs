//Crea un programa que el proceso padre le mande una cadena al proceso
//hijo y este cuente el numero de caracteres
 
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
 
int main ()
{
	pid_t pid;
	pid = fork();
	char buffer[100];
	int p[2], readbytes, longitud;
	pipe(p);
	
	if (pid == 0)
	{
		close(p[1]);
		while((readbytes = read(p[0],buffer,100))>0)
		{
			printf("El numero de caracteres de la palabra %s es: %ld",buffer, strlen(buffer));
		}
		//Opcion 2
		//while (buffer[i] == '\0') {
		//longitud++;
		//i++;
		//}
		//printf ("La cadena que me ha mandado mi padre tiene %d caracteres.\n",longitud);
		close(p[0]);
	}
	else 
	{
		close(p[0]);
		//Opcion 1
		printf("Escribe una cadena.\n");
		scanf("%s",buffer);
		//Opcion 2
		//strcpy(buffer,"Esta es la cadena.\n");
		write(p[1],buffer,strlen(buffer));
		close(p[1]);
	}
	waitpid(pid,NULL,0); 	//Espera a que el pid termine, si no acaba antes de entrar el hijo
}
    
