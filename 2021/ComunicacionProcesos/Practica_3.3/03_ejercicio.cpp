//Haz tres procesos(padre,hijo,nieto) que se comuniquen entre ellos a
//traves de PIPEs. De manera que el padre le mande un nuemro al hijo y este lo 
//traduzca a letra segun sera A-1 B-2 y el nieto vuelva a introducir el numero a numero.
//Si hago un fork() dentro del hijo se crea el nieto
#include <sdtdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define SIZE 250

int main ()
{
	pid_t pid;
	pid_t pid_nieto;
	int padre_hijo[2];
	int hijo_nieto[2];
	int numero,readbytes;
	char buffer[SIZE];
	pipe(padre_hijo);
	pipe(hijo_nieto);

	pid = fork();

	if (pid == 0)
	{
		pid_nieto = fork();
		if (pid_nieto == 0)
		{
			//Nieto
			close(padre_hijo[0]);
			close(padre_hijo[1]);
			close(hijo_nieto[1]);

			while ((readbytes=read(hijo_nieto[0],buffer,SIZE)>0)
			{	
			}
		}
		else
		{
			//Hijo
			close(padre_hijo[1]);
			close(hijo_nieto[0]);

			while((readbytes = read(padre_hijo[0],buffer,SIZE)) > 0)
			{
				printf("Los datos que sacamos de la tuberia padre_hijo son %s\n", buffer);
				//Transform cadena de caracteres a un solo numero
				numero = atoi(buffer);
				switch (numero)
				{
					case 0: strcpy(buffer, "a");
									break;
					
					case 1: strcpy(buffer, "b");
									break;

					case 2: strcpy(buffer, "c");
									break;
				
					case 3: strcpy(buffer, "d");
									break;

					case 4: strcpy(buffer, "e");
									break;
			
					case 5: strcpy(buffer, "f");
									break;
			
					case 6: strcpy(buffer, "g");
									break;
	
					case 7: strcpy(buffer, "h");
									break;
				
					case 8: strcpy(buffer, "i");
									break;
	
					case 9: strcpy(buffer, "j");
									break;
					default:
									strcpy(buffer, "El dato no es valido.\n");
				}
			}
			close(padre_hijo[0]);
			//Escribir hijo al nieto
			write(hijo_nieto[1],buffer,strlen(buffer));
			close(hijo_nieto[1]);
			waitpid(pid_nieto,NULL,0);
			exit(0);
		}
		
	}
	else
	{
		//Padre
		close(padre_hijo[0]);
		close(hijo_nieto[0]);
		close(hijo_nieto[1]);
		printf("Escribe un numero para mandar al HIJO: \n");
		scanf("%d\n",&numero);
		//Transformar un entero a una cadena de caracteres
		sprintf(buffer, "%d", numero);
		write (padre_hijo[1],buffer,strlen(buffer));
		close(padre_hijo[1]);
		//Para que espero al pid del hijo
		waitpid(pid_hijo, NULL, 0);
		exit(0);
	}

}
