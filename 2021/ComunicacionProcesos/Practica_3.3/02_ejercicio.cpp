//Modifica el programa anterior para que el hijo devuelva la cadena que le ha 
//mandado el padre dada la vuelta con otra tuberia. 
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
int main ()
{
	pid_t pid;
	pid = fork();
	char bufferInvert[100];
	char buffer[100];
	int p[2],p2[2], readbytes,readbytes2, longitud;
	pipe(p);
	pipe(p2);
	char aux;
	if (pid == 0)
	{
		close(p[1]);	//Cerramos el lado de escritura del pipe que no utilizamos
		close(p2[0]);
		while((readbytes = read(p[0],buffer,100))>0)
		{
			printf("El numero de caracteres de la palabra %s es: %ld",buffer, strlen(buffer));
		}
		strcpy(bufferInvert,buffer);
		int longitud = strlen(bufferInvert);
		for (int i=0; i<longitud; i++)
		{
			aux = bufferInvert[i];
			bufferInvert[i] = bufferInvert[longitud-i-1];
			bufferInvert[longitud-i-1] = aux;
		}
		write(p2[1],bufferInvert,strlen(bufferInvert));
		close(p2[1]);

		//Opcion 2
		//while (buffer[i] == '\0') {
		//longitud++;
		//i++;
		//}
		close(p[0]);
	}
	else 
	{
		close(p[0]);
		close(p2[1]);
		//Opcion 1
		printf("Escribe una cadena.\n");
		scanf("%s",buffer);
		//Opcion 2
		//strcpy(buffer,"Esta es la cadena.\n");
		write(p[1],buffer,strlen(buffer));
		close(p[1]);
		while ((readbytes2 = read(p2[0],bufferInvert,100))>0)
		{
			printf ("La cadena invertida es: %s",bufferInvert);
		}
		close(p2[0]);
	}
	waitpid(pid,NULL,0); 	//Espera a que el pid termine, si no acaba antes de entrar el hijo
	
	exit(0);
}
    
