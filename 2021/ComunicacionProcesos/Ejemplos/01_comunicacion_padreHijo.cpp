//Entrada bidireccional


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SIZE 512

int main ()
{	
	pid_t pid;
	int p[2], readbytes;
	pipe(p);

	pid = fork();

	if (pid == 0)
	{
		close(p[1]);	
	}
	return 0;
}
